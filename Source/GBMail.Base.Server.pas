unit GBMail.Base.Server;

interface

uses GBMail.Interfaces, System.Classes, System.SysUtils;

const
  BASE_CONNECT_TIME_OUT = 10000;
  BASE_READ_TIME_OUT    = 10000;

type TGBMailServerBase = class (TInterfacedObject, IGBMailServer)

  private
    [Weak]
    FParent   : IGBMail;
    FHost     : String;
    FPort     : Integer;
    FUsername : String;
    FPassword : String;
    FUseSSL   : Boolean;
    FUseTLS   : Boolean;
    FRequireAuthentication: Boolean;
    FConnectTimeOut : Integer;
    FReadTimeOut    : Integer;

  public
    function &Begin: IGBMailServer;
    function &End  : IGBMail;

    function Host           (Value: String): IGBMailServer; overload;
    function Port           (Value: Integer): IGBMailServer; overload;
    function Username       (Value: String): IGBMailServer; overload;
    function Password       (Value: String): IGBMailServer; overload;
    function UseSSL         (Value: Boolean): IGBMailServer; overload;
    function UseTLS         (Value: Boolean): IGBMailServer; overload;
    function ConnectTimeOut (Value: Integer): IGBMailServer; overload;
    function ReadTimeOut    (Value: Integer): IGBMailServer; overload;

    function Host           : String;  overload;
    function Port           : Integer; overload;
    function Username       : String;  overload;
    function Password       : String;  overload;
    function UseSSL         : Boolean; overload;
    function UseTLS         : Boolean; overload;
    function ConnectTimeOut : Integer; overload;
    function ReadTimeOut    : Integer; overload;

    function RequireAuthentication(Value: Boolean): IGBMailServer; overload;
    function RequireAuthentication: Boolean; overload;

    constructor create(GBMail: IGBMail);
    class function New(GBMail: IGBMail): IGBMailServer;
end;

implementation

{ TGBMailServerBase }

function TGBMailServerBase.&Begin: IGBMailServer;
begin
  result := Self;
end;

function TGBMailServerBase.&End: IGBMail;
begin
  result := FParent;
end;

function TGBMailServerBase.ConnectTimeOut: Integer;
begin
  result := FConnectTimeOut;
end;

function TGBMailServerBase.ConnectTimeOut(Value: Integer): IGBMailServer;
begin
  Result := Self;
  FConnectTimeOut := Value;
end;

constructor TGBMailServerBase.create(GBMail: IGBMail);
begin
  Self.FParent                := GBMail;
  Self.FUseSSL                := True;
  Self.FUseTLS                := True;
  Self.FRequireAuthentication := True;
  Self.FConnectTimeOut        := BASE_CONNECT_TIME_OUT;
  Self.FReadTimeOut           := BASE_READ_TIME_OUT;
end;

function TGBMailServerBase.Host(Value: String): IGBMailServer;
begin
  result := Self;
  FHost  := Value;
end;

function TGBMailServerBase.Host: String;
begin
  result := FHost;
end;

class function TGBMailServerBase.New(GBMail: IGBMail): IGBMailServer;
begin
  result := Self.create(GBMail);
end;

function TGBMailServerBase.Password(Value: String): IGBMailServer;
begin
  result    := Self;
  FPassword := Value;
end;

function TGBMailServerBase.Password: String;
begin
  result := FPassword;
end;

function TGBMailServerBase.Port: Integer;
begin
  result := FPort;
end;

function TGBMailServerBase.Port(Value: Integer): IGBMailServer;
begin
  result := Self;
  FPort  := Value;
end;

function TGBMailServerBase.ReadTimeOut: Integer;
begin
  result := FReadTimeOut;
end;

function TGBMailServerBase.ReadTimeOut(Value: Integer): IGBMailServer;
begin
  Result       := Self;
  FReadTimeOut := Value;
end;

function TGBMailServerBase.RequireAuthentication: Boolean;
begin
  result := FRequireAuthentication;
end;

function TGBMailServerBase.RequireAuthentication(Value: Boolean): IGBMailServer;
begin
  result                 := Self;
  FRequireAuthentication := Value;
end;

function TGBMailServerBase.Username(Value: String): IGBMailServer;
begin
  result    := Self;
  FUsername := Value;
end;

function TGBMailServerBase.Username: String;
begin
  result := FUsername;
end;

function TGBMailServerBase.UseSSL(Value: Boolean): IGBMailServer;
begin
  result  := Self;
  FUseSSL := Value;
end;

function TGBMailServerBase.UseSSL: Boolean;
begin
  result := FUseSSL;
end;

function TGBMailServerBase.UseTLS(Value: Boolean): IGBMailServer;
begin
  result  := Self;
  FUseTLS := Value;
end;

function TGBMailServerBase.UseTLS: Boolean;
begin
  result := FUseTLS;
end;

end.
