unit GBMail.Factory;

interface

uses GBMail.Interfaces, GBMail.Indy;

type TGBMailFactory = class(TInterfacedObject, IGBMailFactory)

  public
    class function New: IGBMailFactory;
    function createMailDefault : IGBMail;
    function createMailIndy    : IGBMail;

end;

implementation

{ TGBMailFactory }

function TGBMailFactory.createMailDefault: IGBMail;
begin
  result := createMailIndy;
end;

function TGBMailFactory.createMailIndy: IGBMail;
begin
  result := TGBMailIndy.New;
end;

class function TGBMailFactory.New: IGBMailFactory;
begin
  result := Self.Create;
end;

end.
