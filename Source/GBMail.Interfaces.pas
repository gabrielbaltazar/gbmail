unit GBMail.Interfaces;

interface

uses SysUtils, Classes;

type
  EGBMailException = class(Exception);
  IGBMailServer = interface;

  IGBMail = interface
    ['{2C44519A-0B71-449D-BC2D-6D429EA37BE3}']
    function Server: IGBMailServer;

    function From           (Address: string; Name: String = ''): IGBMail;
    function AddRecipient   (Address: string; Name: string = ''): IGBMail;
    function AddCcRecipient (Address: string; Name: string = ''): IGBMail;
    function AddBccRecipient(Address: string; Name: string = ''): IGBMail;
    function AddAttachment  (FileName: String): IGBMail; overload;
    function AddAttachment  (Stream: TMemoryStream; FileName: String): IGBMail; overload;

    function AddHtmlImage   (FileName: String; out ID: String): IGBMail; overload;
    function AddHtmlImage   (Image: TMemoryStream; out ID: String): IGBMail; overload;

    function UseHtml(Value: Boolean) : IGBMail;
    function Subject(Value: String)  : IGBMail;
    function Message(Value: String)  : IGBMail; overload;
    function Message(Value: TStrings): IGBMail; overload;

    function Send: IGBMail;
  end;

  IGBMailServer = interface
    ['{05CBBF52-507A-4DD0-9163-F389DB4EB7D5}']

    function &Begin: IGBMailServer;
    function &End  : IGBMail;

    function Host           (Value: String) : IGBMailServer; overload;
    function Port           (Value: Integer): IGBMailServer; overload;
    function Username       (Value: String) : IGBMailServer; overload;
    function Password       (Value: String) : IGBMailServer; overload;
    function UseSSL         (Value: Boolean): IGBMailServer; overload;
    function UseTLS         (Value: Boolean): IGBMailServer; overload;
    function ConnectTimeOut (Value: Integer): IGBMailServer; overload;
    function ReadTimeOut    (Value: Integer): IGBMailServer; overload;

    function Host           : String;  overload;
    function Port           : Integer; overload;
    function Username       : String;  overload;
    function Password       : String;  overload;
    function UseSSL         : Boolean; overload;
    function UseTLS         : Boolean; overload;
    function ConnectTimeOut : Integer; overload;
    function ReadTimeOut    : Integer; overload;

    function RequireAuthentication(Value: Boolean): IGBMailServer; overload;
    function RequireAuthentication: Boolean; overload;
  end;

  IGBMailFactory = interface
    ['{1E6828E6-3F29-44D5-A469-760C24474990}']
    function createMailDefault: IGBMail;
    function createMailIndy   : IGBMail;
  end;

function GBMailFactory: IGBMailFactory;
function GBMailDefault: IGBMail;

implementation

uses
  GBMail.Factory;

function GBMailDefault: IGBMail;
begin
  result := GBMailFactory.createMailDefault;
end;

function GBMailFactory: IGBMailFactory;
begin
  result := TGBMailFactory.New;
end;

end.
