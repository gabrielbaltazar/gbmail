unit GBMail.Mail.Base;

interface

uses GBMail.Interfaces, GBMail.Base.Server, System.Classes, System.SysUtils,
     System.StrUtils;

type TGBMailBase = class abstract(TInterfacedObject, IGBMail)

  private
    FServer        : IGBMailServer;
    FFromAddress   : string;
    FFromName      : string;
    FSubject       : string;
    FUseHtml       : Boolean;
    FRecipients    : TStrings;
    FCcRecipients  : TStrings;
    FBCcRecipients : TStrings;
    FMessage       : TStrings;
  public
    function Server: IGBMailServer;

    function From           (Address: string; Name: String = ''): IGBMail;
    function AddRecipient   (Address: string; Name: string = ''): IGBMail;
    function AddCcRecipient (Address: string; Name: string = ''): IGBMail;
    function AddBccRecipient(Address: string; Name: string = ''): IGBMail;

    function UseHtml(Value: Boolean = False): IGBMail;
    function Subject(Value: String)  : IGBMail;
    function Message(Value: String)  : IGBMail; overload;
    function Message(Value: TStrings): IGBMail; overload;

    function Send: IGBMail; virtual; abstract;

    constructor create;
    class function New: IGBMail;
    destructor Destroy; override;

end;

implementation

{ TGBMailBase }

function TGBMailBase.AddBccRecipient(Address, Name: string): IGBMail;
begin
  result := Self;
  FBCcRecipients.Values[Address] := IfThen(Name.IsEmpty, Address, Name);
end;

function TGBMailBase.AddCcRecipient(Address, Name: string): IGBMail;
begin
  result := Self;
  FCcRecipients.Values[Address] := IfThen(Name.IsEmpty, Address, Name);
end;

function TGBMailBase.AddRecipient(Address, Name: string): IGBMail;
begin
  result := Self;
  FRecipients.Values[Address] := IfThen(Name.IsEmpty, Address, Name);
end;

constructor TGBMailBase.create;
begin

end;

destructor TGBMailBase.Destroy;
begin

  inherited;
end;

function TGBMailBase.From(Address, Name: String): IGBMail;
begin

end;

function TGBMailBase.Message(Value: String): IGBMail;
begin

end;

function TGBMailBase.Message(Value: TStrings): IGBMail;
begin

end;

class function TGBMailBase.New: IGBMail;
begin

end;

function TGBMailBase.Server: IGBMailServer;
begin

end;

function TGBMailBase.Subject(Value: String): IGBMail;
begin

end;

function TGBMailBase.UseHtml(Value: Boolean): IGBMail;
begin

end;

end.
